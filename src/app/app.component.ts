import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  private chartData: Array<any>;
  private tmpMap: Map<any,any>;
  private page: number;

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.page = 1;
    setTimeout(() => {
      this.tmpMap = new Map();
      this.scanPages();
      setInterval(() => {
        this.chartData = [];
        this.chartData = Array.from(this.tmpMap);
        console.log(this.chartData);
      }, 1000);
    }, 1000);
  }

  scanPages() {
    this.generateData().then( promise => {
        this.page = this.page + 1;
        this.scanPages();
      }
    );
  }

  generateData() {
    let promise = new Promise((resolve, reject) => {
      this.http.get('https://api.producthunt.com/v1/comments?page='+this.page, 
      {headers: { Authorization: 'Bearer 21b76d0a8b5d3a1e64a14e65e619da6b0c0afe8268a28df87c4b8796d58be35c'}})
      .toPromise()
      .then(res => {
        let comments = res["comments"];
        for(let i=0; i < comments.length; i++)
        {
          let date = comments[i].created_at.substring(0,10);
          let stop_at = new Date();
          stop_at.setDate(stop_at.getDate() - 7);
          let stop_date = stop_at.toISOString().substring(0,10);
          if(date == stop_date){
            reject();
          }
          else
          {
            if(isNaN(this.tmpMap.get(date))) this.tmpMap.set(date,0);
            this.tmpMap.set(date, this.tmpMap.get(date)+1);
            resolve();
          }
        }
      });
    });
    
    return promise;
  }
}